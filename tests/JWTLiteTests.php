<?php

namespace Billy\JWTLite;

require_once dirname(__FILE__) . '/../src/JWTLite.php';

class JWTLiteTests extends \PHPUnit\Framework\TestCase {

    public static $testToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0=.eyJ1c2VyIjoidGVzdC11c2VyIn0=.';
    public static $testTokenSecret = "YoullNeverGuess";
    public static $testTokenPayload = ['user' => 'test-user'];

    public function testEncode() {
        self::assertEquals(
            self::$testToken,
            JWTLite::encode(self::$testTokenPayload, self::$testTokenSecret)
        );
    }

    public function testDecode() {
        self::assertEquals(
            self::$testTokenPayload,
            JWTLite::decode(self::$testToken, self::$testTokenSecret)
        );
    }

    public function testInvalidToken() {
        $this->expectException("\Exception");
        JWTLite::decode('notarealtoken', self::$testTokenSecret);
    }
}