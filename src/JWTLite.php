<?php

namespace Billy\JWTLite;

/**
 * Class JWTLite
 * @package Billy\JWTLite
 */
class JWTLite {

    public static $defaultHeader = [
        'typ' => 'JWT',
        'alg' => 'none'
    ];

    /**
     * @param $claims
     * @param $secret
     * @param array $header
     * @return string A JWT Tokenized version of the given claims
     */
    public static function encode($claims, $secret, $header = []) {
        $header = array_merge(self::$defaultHeader, $header);
        //Apply any overrides of the default values
        $encodedHeader = self::encodeJWTPart($header);

        $encodedClaims = self::encodeJWTPart($claims);

        $signature = self::getSignature($encodedHeader, $encodedClaims, $secret, $header['alg']);

        return sprintf("%s.%s.%s", $encodedHeader, $encodedClaims, $signature);
    }

    /**
     * I realise this method serves very little purpose given the simplified requirements
     * of this particular exercise (i.e. encryption not required) but I thought it might
     * be a good idea to "stub out" a method that could be used for future features.
     *
     * @param $headerPart
     * @param $payloadPart
     * @param $secret
     * @param $algorithm
     * @return string
     */
    public static function getSignature($headerPart, $payloadPart, $secret, $algorithm) {
        switch($algorithm) {
            default:
                return '';
        }
    }

    /**
     * @param $token
     * @param $secret
     * @return mixed
     * @throws \Exception
     */
    public static function decode($token, $secret) {
        $parts = explode(".", $token);

        if(count($parts) !== 3) {
            throw new \Exception('Invalid token');
        }

        $header = self::decodeJWTPart($parts[0]);
        $payload = self::decodeJWTPart($parts[1]);
        $signature = $parts[2];

        if(self::getSignature($parts[0], $parts[1], $secret, $header['alg']) != $signature) {
            throw new \Exception('Invalid token signature');
        }

        return $payload;
    }

    /**
     * @param $content
     * @return string
     * @throws \Exception
     */
    public static function encodeJWTPart($content) {
        $json = json_encode($content);
        if($error = json_last_error()) {
            throw new \Exception(sprintf('Error encoding content to JSON: %s', $error));
        }
        return base64_encode($json);
    }

    /**
     * @param $content
     * @return mixed
     * @throws \Exception
     */
    public static function decodeJWTPart($content) {
        $json = base64_decode($content);

        $data = json_decode($json, true);
        if($error = json_last_error()) {
            throw new \Exception(sprintf('Error decoding from JSON: %s', $error));
        }
        return $data;
    }
}