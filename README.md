#### Hi! Welcome to my extremely lite JWT token wrangler.

I hope you like it! I realise it's very bare bones, I tried to keep as more an example of code style.

A production ready version would obvioulsy have a lot more validation code, and, you would hope a bunch more features. I dropped a method in there to kind of demonstrate one place where features could be added, I hope that doesn't make me too much of a try hard ;)

To test just run:

```bash
phpunit tests/JWTLiteTests.php
```